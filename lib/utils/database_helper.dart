import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:not_sepeti/models/kategori.dart';
import 'package:not_sepeti/models/not.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DatabaseHelper {
  static DatabaseHelper _databaseHelper;
  static Database _database;

  factory DatabaseHelper() {
    if (_databaseHelper == null) _databaseHelper = DatabaseHelper._internal();
    return _databaseHelper;
  }

  DatabaseHelper._internal();

  Future<Database> _getDatabase() async {
    if (_database == null) {
      _database = await _initializeDatabase();
    }
    return _database;
  }

  Future<Database> _initializeDatabase() async {
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "notlar.db");
    debugPrint(path);

    // Check if the database exists
    var exists = await databaseExists(path);

    if (!exists) {
      // Should happen only the first time you launch your application
      print("Creating new copy from asset");

      // Make sure the parent directory exists
      try {
        await Directory(dirname(path)).create(recursive: true);
      } catch (_) {}

      // Copy from asset
      ByteData data = await rootBundle.load(join("assets", "notlar.db"));
      List<int> bytes =
          data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);

      // Write and flush the bytes written
      await File(path).writeAsBytes(bytes, flush: true);
    } else {
      print("Opening existing database");
    }
    // open the database
    return await openDatabase(path, readOnly: false);
  }

  //Kategori CRUD işlemleri
  Future<List<Map<String, dynamic>>> kategorileriGetir() async {
    var db = await _getDatabase();
    return await db.query("kategori");
  }

  Future<List<Kategori>> kategoriListesiniGetir() async {
    var kategorilerMapListesi = await kategorileriGetir();
    var kategoriListesi = List<Kategori>();
    kategorilerMapListesi.forEach((element) {
      kategoriListesi.add(Kategori.fromMap(element));
    });
    return kategoriListesi;
  }

  Future<int> kategoriEkle(Kategori kategori) async {
    var db = await _getDatabase();
    return await db.insert("kategori", kategori.toMap());
  }

  Future<int> kategoriGuncelle(Kategori kategori) async {
    var db = await _getDatabase();
    return await db.update("kategori", kategori.toMap(),
        where: "kategoriID = ?", whereArgs: [kategori.kategoriID]);
  }

  Future<int> kategoriSil(int kategoriID) async {
    var db = await _getDatabase();
    var sonuc = await db
        .delete("kategori", where: "kategoriID = ?", whereArgs: [kategoriID]);
    return sonuc;
  }

  //Not CRUD işlemleri
  Future<List<Map<String, dynamic>>> notlariGetir() async {
    var db = await _getDatabase();
    return await db.rawQuery(
        "select * from 'not' inner join kategori on kategori.kategoriID = 'not'.kategoriID order by notID desc");
  }

  Future<List<Not>> notlarListesiGetir() async {
    var notlarMapListesi = await notlariGetir();
    var notListesi = List<Not>();
    for (Map map in notlarMapListesi) {
      notListesi.add(Not.fromMap(map));
    }
    return notListesi;
  }

  Future<int> notEkle(Not not) async {
    var db = await _getDatabase();
    return await db.insert("not", not.toMap());
  }

  Future<int> notGuncelle(Not not) async {
    var db = await _getDatabase();
    return await db
        .update("not", not.toMap(), where: "notID = ?", whereArgs: [not.notID]);
  }

  Future<int> notSil(int notID) async {
    var db = await _getDatabase();
    return await db.delete("not", where: "notID = ?", whereArgs: [notID]);
  }

  String dateFormat(DateTime tm) {
    DateTime today = new DateTime.now();
    Duration oneDay = new Duration(days: 1);
    Duration twoDay = new Duration(days: 2);
    Duration oneWeek = new Duration(days: 7);
    String month;
    switch (tm.month) {
      case 1:
        month = "Ocak";
        break;
      case 2:
        month = "Şubat";
        break;
      case 3:
        month = "Mart";
        break;
      case 4:
        month = "Nisan";
        break;
      case 5:
        month = "Mayıs";
        break;
      case 6:
        month = "Haziran";
        break;
      case 7:
        month = "Temmuz";
        break;
      case 8:
        month = "Ağustos";
        break;
      case 9:
        month = "Eylük";
        break;
      case 10:
        month = "Ekim";
        break;
      case 11:
        month = "Kasım";
        break;
      case 12:
        month = "Aralık";
        break;
    }

    Duration difference = today.difference(tm);

    if (difference.compareTo(oneDay) < 1) {
      return "Bugün";
    } else if (difference.compareTo(twoDay) < 1) {
      return "Dün";
    } else if (difference.compareTo(oneWeek) < 1) {
      switch (tm.weekday) {
        case 1:
          return "Pazartesi";
        case 2:
          return "Salı";
        case 3:
          return "Çarşamba";
        case 4:
          return "Perşembe";
        case 5:
          return "Cuma";
        case 6:
          return "Cumartesi";
        case 7:
          return "Pazar";
      }
    } else if (tm.year == today.year) {
      return '${tm.day} $month';
    } else {
      return '${tm.day} $month ${tm.year}';
    }
    return "";
  }
}
