import 'package:flutter/material.dart';
import 'package:not_sepeti/models/kategori.dart';
import 'package:not_sepeti/utils/database_helper.dart';

class Kategoriler extends StatefulWidget {
  Kategoriler({Key key}) : super(key: key);

  @override
  _KategorilerState createState() => _KategorilerState();
}

class _KategorilerState extends State<Kategoriler> {
  List<Kategori> kategoriler;
  DatabaseHelper dbHelper;
  var _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    dbHelper = DatabaseHelper();
  }

  @override
  Widget build(BuildContext context) {
    if (kategoriler == null) {
      kategoriler = List<Kategori>();
      kategoriListesiGuncelle();
    }
    return Container(
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text("Kategoriler"),
        ),
        body: ListView.builder(
            itemCount: kategoriler.length,
            itemBuilder: (context, index) {
              return ListTile(
                onTap: () => _kategoriGuncelle(kategoriler[index], context),
                title: Text(kategoriler[index].kategoriBaslik),
                trailing: IconButton(
                    icon: Icon(Icons.delete),
                    onPressed: () =>
                        _kategoriSil(kategoriler[index].kategoriID)),
                leading: Icon(Icons.category),
              );
            }),
      ),
    );
  }

  void kategoriListesiGuncelle() {
    dbHelper.kategoriListesiniGetir().then((values) {
      setState(() {
        kategoriler = values;
        print(kategoriler.toString());
      });
    });
  }

  _kategoriSil(int kategoriID) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          title: Text("Eminmisiniz?"),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                  "Kategoriyi sildiğinizde bununla ilgili bğtğn notlar silinecek. Emin misiniz?"),
              ButtonBar(
                children: <Widget>[
                  FlatButton(
                      onPressed: () => Navigator.pop(context),
                      child: Text("Vazgeç")),
                  FlatButton(
                      onPressed: () {
                        dbHelper.kategoriSil(kategoriID).then((value) {
                          print("Silinen kategori: " + value.toString());
                          if (value != 0) {
                            setState(() {
                              kategoriListesiGuncelle();
                            });
                            Navigator.pop(context);
                          }
                        });
                      },
                      child: Text(
                        "Sil",
                        style: TextStyle(color: Colors.redAccent),
                      )),
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  _kategoriGuncelle(Kategori guncellenecekKategori, BuildContext context) {
    _kategoriGuncelleDialog(context, guncellenecekKategori);
  }

  void _kategoriGuncelleDialog(
      BuildContext context, Kategori guncellenecekKategori) {
    var formKey = GlobalKey<FormState>();
    String guncellenecekKategoriAdi;

    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return SimpleDialog(
          title: Text(
            "Kategori Güncelle",
            style: TextStyle(color: Theme.of(context).primaryColor),
          ),
          children: <Widget>[
            Form(
              key: formKey,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  initialValue: guncellenecekKategori.kategoriBaslik,
                  onSaved: (newValue) {
                    guncellenecekKategoriAdi = newValue;
                  },
                  decoration: InputDecoration(
                    labelText: "Kategori Adı",
                    border: OutlineInputBorder(),
                  ),
                  validator: (value) {
                    if (value.length < 3) return "En az 3 karakter olmalı";
                  },
                ),
              ),
            ),
            ButtonBar(
              alignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                RaisedButton(
                  onPressed: () => Navigator.pop(context),
                  color: Colors.orangeAccent,
                  child: Text(
                    "Vazgeç",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                RaisedButton(
                  onPressed: () {
                    if (formKey.currentState.validate()) {
                      formKey.currentState.save();
                      dbHelper
                          .kategoriGuncelle(Kategori.withID(
                              guncellenecekKategori.kategoriID,
                              guncellenecekKategoriAdi))
                          .then((value) {
                        if (value != 0) {
                          _showSnackBar("Kategori güncellendi");
                          Navigator.pop(context);
                          kategoriListesiGuncelle();
                        }
                      });
                    }
                  },
                  color: Theme.of(context).primaryColor,
                  child: Text(
                    "Güncelle",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ],
            ),
          ],
        );
      },
    );
  }

  void _showSnackBar(String msg) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(msg),
      duration: Duration(seconds: 2),
    ));
  }
}
