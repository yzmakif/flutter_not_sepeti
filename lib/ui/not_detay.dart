import 'package:flutter/material.dart';
import 'package:not_sepeti/models/kategori.dart';
import 'package:not_sepeti/models/not.dart';
import 'package:not_sepeti/utils/database_helper.dart';

class NotDetay extends StatefulWidget {
  String baslik;
  Not not;
  NotDetay({this.baslik, this.not});

  @override
  _NotDetayState createState() => _NotDetayState();
}

class _NotDetayState extends State<NotDetay> {
  var _formKey = GlobalKey<FormState>();
  var _scaffoldKey = GlobalKey<ScaffoldState>();
  List<Kategori> kategoriler;
  DatabaseHelper dbHelper;
  int kategoriID;
  int secilenOncelik;
  static var _oncelik = ["Düşük", "Orta", "Yüksek"];
  String notBaslik;

  String notIcerik;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    kategoriID = widget.not != null ? widget.not.kategoriID : 1;
    secilenOncelik = widget.not != null ? widget.not.notOncelik : 0;
    kategoriler = [];
    dbHelper = DatabaseHelper();
    dbHelper.kategorileriGetir().then((values) {
      values.forEach((kategori) {
        kategoriler.add(Kategori.fromMap(kategori));
      });
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          widget.baslik,
        ),
      ),
      body: kategoriler.length <= 0
          ? Center(
              child: CircularProgressIndicator(),
            )
          : SingleChildScrollView(
              primary: false,
              child: Container(
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Expanded(
                            flex: 2,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 12),
                              child: Text(
                                "Kategori :",
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 5,
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 2, horizontal: 12),
                              margin: EdgeInsets.all(12),
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: Theme.of(context).primaryColor,
                                    width: 2),
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton(
                                  //isExpanded: true,
                                  items: _kategoriItemleriOlustur(),
                                  value: kategoriID,
                                  onChanged: (value) {
                                    setState(() {
                                      kategoriID = value;
                                    });
                                  },
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            flex: 2,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 12),
                              child: Text(
                                "Öncelik :",
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 5,
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 2, horizontal: 12),
                              margin: EdgeInsets.all(12),
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: Theme.of(context).primaryColor,
                                    width: 2),
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton(
                                  //isExpanded: true,
                                  items: _oncelik
                                      .map(
                                        (String s) => DropdownMenuItem<int>(
                                          value: _oncelik.indexOf(s),
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Text(
                                              s,
                                              style: TextStyle(fontSize: 20),
                                            ),
                                          ),
                                        ),
                                      )
                                      .toList(),
                                  value: secilenOncelik,
                                  onChanged: (value) {
                                    setState(() {
                                      secilenOncelik = value;
                                    });
                                  },
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: TextFormField(
                          initialValue:
                              widget.not != null ? widget.not.notBaslik : "",
                          validator: (value) {
                            if (value.length < 3)
                              return "En az 3 karakter olmali";
                          },
                          onSaved: (newValue) {
                            setState(() {
                              notBaslik = newValue;
                            });
                          },
                          decoration: InputDecoration(
                            hintText: "Not başlığını giriniz",
                            labelText: "Not Başlığı",
                            border: OutlineInputBorder(),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: TextFormField(
                          initialValue:
                              widget.not != null ? widget.not.notIcerik : "",
                          onSaved: (newValue) {
                            setState(() {
                              notIcerik = newValue;
                            });
                          },
                          maxLines: 5,
                          decoration: InputDecoration(
                            hintText: "Not içeriğini giriniz",
                            labelText: "Not İçeriği",
                            border: OutlineInputBorder(),
                          ),
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: Container(
                              height: 45,
                              padding: EdgeInsets.symmetric(horizontal: 12),
                              child: RaisedButton(
                                onPressed: () => Navigator.pop(context),
                                child: Text(
                                  "Vazgeç",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ),
                                color: Colors.grey,
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Container(
                              height: 45,
                              padding: EdgeInsets.symmetric(horizontal: 12),
                              child: RaisedButton(
                                onPressed: () {
                                  if (_formKey.currentState.validate()) {
                                    _formKey.currentState.save();
                                    var suan = DateTime.now();
                                    if (widget.not == null) {
                                      dbHelper
                                          .notEkle(Not(
                                        kategoriID,
                                        notBaslik,
                                        notIcerik,
                                        suan.toString(),
                                        secilenOncelik,
                                      ))
                                          .then((value) {
                                        if (value != 0) Navigator.pop(context);
                                      });
                                    } else {
                                      dbHelper
                                          .notGuncelle(Not.withID(
                                        widget.not.notID,
                                        kategoriID,
                                        notBaslik,
                                        notIcerik,
                                        suan.toString(),
                                        secilenOncelik,
                                      ))
                                          .then((value) {
                                        if (value != 0) Navigator.of(context).pop();
                                      });
                                    }
                                  }
                                },
                                child: Text(
                                  "Kaydet",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ),
                                color: Theme.of(context).primaryColor,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
    );
  }

  List<DropdownMenuItem<int>> _kategoriItemleriOlustur() {
    List<DropdownMenuItem<int>> listItems = [];
    return kategoriler
        .map(
          (Kategori kategori) => DropdownMenuItem<int>(
            value: kategori.kategoriID,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                kategori.kategoriBaslik,
                style: TextStyle(fontSize: 20),
              ),
            ),
          ),
        )
        .toList();
  }

  void _showSnackBar(String msg) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(msg),
      duration: Duration(seconds: 2),
    ));
  }
}
/*
Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            kategoriler.length <= 0
                ? CircularProgressIndicator()
                : Container(
                    padding: EdgeInsets.symmetric(vertical: 4, horizontal: 12),
                    margin: EdgeInsets.all(12),
                    decoration: BoxDecoration(
                      border: Border.all(
                          color: Theme.of(context).primaryColor, width: 2),
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<int>(
                        isExpanded: true,
                        items: _kategoriItemleriOlustur(),
                        value: kategoriID,
                        onChanged: (value) {
                          setState(() {
                            kategoriID = value;
                          });
                        },
                      ),
                    ),
                  ),
          ],
        ),
      ),
 */
