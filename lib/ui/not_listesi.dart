import 'dart:io';

import 'package:flutter/material.dart';
import 'package:not_sepeti/models/kategori.dart';
import 'package:not_sepeti/models/not.dart';
import 'package:not_sepeti/ui/kategori_islemleri.dart';
import 'package:not_sepeti/ui/not_detay.dart';
import 'package:not_sepeti/utils/database_helper.dart';

class NotListesi extends StatelessWidget {
  DatabaseHelper dbHelper = DatabaseHelper();
  var _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Not Sepeti"),
        actions: <Widget>[
          PopupMenuButton(itemBuilder: (context) {
            return [
              PopupMenuItem(
                child: ListTile(
                  leading: Icon(Icons.category),
                  title: Text("Kategoriler"),
                  onTap: () {
                    Navigator.pop(context);
                    _kategorilerSayfasinaGit(context);
                  },
                ),
              ),
            ];
          })
        ],
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          FloatingActionButton(
            onPressed: () {
              kategoriEkleDialog(context);
            },
            heroTag: "KategoriEkle",
            tooltip: "Kategori ekle",
            child: Icon(Icons.playlist_add),
            mini: true,
          ),
          FloatingActionButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => NotDetay(baslik: "Yeni Not")));
            },
            heroTag: "NotEkle",
            tooltip: "Not ekle",
            child: Icon(Icons.add),
          ),
        ],
      ),
      body: Notlar(),
    );
  }

  void kategoriEkleDialog(BuildContext context) {
    var formKey = GlobalKey<FormState>();
    String kategoriAdi;
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return SimpleDialog(
          title: Text(
            "Kategori Ekle",
            style: TextStyle(color: Theme.of(context).primaryColor),
          ),
          children: <Widget>[
            Form(
              key: formKey,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  onSaved: (newValue) {
                    kategoriAdi = newValue;
                  },
                  decoration: InputDecoration(
                    labelText: "Kategori Adı",
                    border: OutlineInputBorder(),
                  ),
                  validator: (value) {
                    if (value.length < 3) return "En az 3 karakter olmalı";
                  },
                ),
              ),
            ),
            ButtonBar(
              alignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                RaisedButton(
                  onPressed: () => Navigator.pop(context),
                  color: Colors.orangeAccent,
                  child: Text(
                    "Vazgeç",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                RaisedButton(
                  onPressed: () {
                    if (formKey.currentState.validate()) {
                      formKey.currentState.save();
                      dbHelper
                          .kategoriEkle(Kategori(kategoriAdi))
                          .then((value) {
                        if (value > 0)
                          _showSnackBar("$kategoriAdi kategorisi eklendi");
                        else
                          _showSnackBar("Kategori eklenemedi");
                        Navigator.pop(context);
                      });
                    }
                  },
                  color: Theme.of(context).primaryColor,
                  child: Text(
                    "Kaydet",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ],
            ),
          ],
        );
      },
    );
  }

  void _showSnackBar(String msg) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(msg),
      duration: Duration(seconds: 2),
    ));
  }

  void _kategorilerSayfasinaGit(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => Kategoriler(),
    ));
  }
}

class Notlar extends StatefulWidget {
  @override
  _NotlarState createState() => _NotlarState();
}

class _NotlarState extends State<Notlar> {
  List<Not> notlar;
  DatabaseHelper dbHelper;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    notlar = List<Not>();
    dbHelper = DatabaseHelper();
  }

  @override
  Widget build(BuildContext context) {
    print("Yeniden build çalıştı");
    return FutureBuilder(
        future: dbHelper.notlarListesiGetir(),
        builder: (context, AsyncSnapshot<List<Not>> snapShot) {
          if (snapShot.connectionState == ConnectionState.done) {
            notlar = snapShot.data;
            return ListView.builder(
              itemCount: notlar.length,
              itemBuilder: (context, index) {
                return ExpansionTile(
                  leading: _oncelikIconAta(notlar[index].notOncelik),
                  title: Text(notlar[index].notBaslik),
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(4),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "Kategori",
                                  style: TextStyle(color: Colors.redAccent),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  notlar[index].kategoriBaslik,
                                  style: TextStyle(color: Colors.black),
                                ),
                              )
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "Oluşturulma Tarihi",
                                  style: TextStyle(color: Colors.redAccent),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  dbHelper.dateFormat(
                                      DateTime.parse(notlar[index].notTarih)),
                                  style: TextStyle(color: Colors.black),
                                ),
                              )
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              notlar[index].notIcerik,
                              style: TextStyle(fontSize: 18),
                            ),
                          ),
                          ButtonBar(
                            alignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              FlatButton(
                                  onPressed: () => _notSil(notlar[index].notID),
                                  child: Text(
                                    "SİL",
                                    style: TextStyle(color: Colors.redAccent),
                                  )),
                              FlatButton(
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => NotDetay(
                                              baslik: "Notu Düzenle",
                                              not: notlar[index]),
                                        ));
                                  },
                                  child: Text(
                                    "GUNCELLE",
                                    style: TextStyle(color: Colors.redAccent),
                                  )),
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                );
              },
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        });
  }

  _oncelikIconAta(int notOncelik) {
    switch (notOncelik) {
      case 0:
        return CircleAvatar(
          child: Text(
            "AZ",
            style: TextStyle(color: Colors.white),
          ),
          backgroundColor: Colors.redAccent.shade100,
        );
        break;
      case 1:
        return CircleAvatar(
          child: Text(
            "ORTA",
            style: TextStyle(color: Colors.white),
          ),
          backgroundColor: Colors.redAccent.shade200,
        );
        break;
      case 2:
        return CircleAvatar(
          child: Text(
            "ACİL",
            style: TextStyle(color: Colors.white),
          ),
          backgroundColor: Colors.redAccent.shade700,
        );
        break;
      default:
    }
  }

  _notSil(int notID) {
    dbHelper.notSil(notID).then((value) {
      if (value != 0) {
        Scaffold.of(context)
            .showSnackBar(SnackBar(content: Text("Not Silindi")));
        setState(() {});
      }
    });
  }
}
