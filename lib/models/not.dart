import 'dart:convert';

class Not {
  int _notID;
  int _kategoriID;
  String _kategoriBaslik;
  String _notBaslik;
  String _notIcerik;
  String _notTarih;
  int _notOncelik;
  String get kategoriBaslik => _kategoriBaslik;

  set kategoriBaslik(String value) => _kategoriBaslik = value;

  int get notID => _notID;

  set notID(int value) => _notID = value;

  int get kategoriID => _kategoriID;

  set kategoriID(int value) => _kategoriID = value;

  String get notBaslik => _notBaslik;

  set notBaslik(String value) => _notBaslik = value;

  String get notIcerik => _notIcerik;

  set notIcerik(String value) => _notIcerik = value;

  String get notTarih => _notTarih;

  set notTarih(String value) => _notTarih = value;

  int get notOncelik => _notOncelik;

  set notOncelik(int value) => _notOncelik = value;

  Not(
    this._kategoriID,
    this._notBaslik,
    this._notIcerik,
    this._notTarih,
    this._notOncelik,
  );
  Not.withID(
    this._notID,
    this._kategoriID,
    this._notBaslik,
    this._notIcerik,
    this._notTarih,
    this._notOncelik,
  );

  Map<String, dynamic> toMap() {
    return {
      'kategoriID': _kategoriID,
      'notBaslik': _notBaslik,
      'notIcerik': _notIcerik,
      'notTarih': _notTarih,
      'notOncelik': _notOncelik,
    };
  }

  Not.fromMap(Map<String, dynamic> map) {
    this._notID = map['notID'];
    this._kategoriID = map['kategoriID'];
    this._kategoriBaslik = map['kategoriBaslik'];
    this._notBaslik = map['notBaslik'];
    this._notIcerik = map['notIcerik'];
    this._notTarih = map['notTarih'];
    this._notOncelik = map['notOncelik'];
  }

  @override
  String toString() {
    return 'Notlar(notID: $_notID, kategoriID: $_kategoriID, notBaslik: $_notBaslik, notIcerik: $_notIcerik, notTarih: $_notTarih, notOncelik: $_notOncelik)';
  }
}
