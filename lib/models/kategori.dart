import 'dart:convert';

class Kategori {
  int _kategoriID;
  String _kategoriBaslik;

  Kategori(
    this._kategoriBaslik,
  );

  Kategori.withID(
    this._kategoriID,
    this._kategoriBaslik,
  );
  int get kategoriID => _kategoriID;

  set kategoriID(int value) => _kategoriID = value;

  String get kategoriBaslik => _kategoriBaslik;

  set kategoriBaslik(String value) => _kategoriBaslik = value;

  Map<String, dynamic> toMap() {
    return {
      'kategoriBaslik': _kategoriBaslik,
    };
  }

  static Kategori fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Kategori.withID(
      map['kategoriID'],
      map['kategoriBaslik'],
    );
  }

  @override
  String toString() =>
      'Kategori(_kategoriID: $_kategoriID, _kategoriBaslik: $_kategoriBaslik)';
}
